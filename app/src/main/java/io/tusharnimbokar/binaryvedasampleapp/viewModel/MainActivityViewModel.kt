package io.tusharnimbokar.binaryvedasampleapp.viewModel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.tusharnimbokar.binaryvedasampleapp.repository.MainRepository
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel@Inject constructor(private val mainRepository: MainRepository):
    ViewModel() {
}