package io.tusharnimbokar.binaryvedasampleapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.tusharnimbokar.binaryvedasampleapp.R

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }
}