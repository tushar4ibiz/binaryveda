package io.tusharnimbokar.binaryvedasampleapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import io.tusharnimbokar.binaryvedasampleapp.R

class SplashActivity : AppCompatActivity() {
    val SPLASH_DELAY: Long = 3000 //Time delay to remove splash screen

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        /* Add Delay to show Login/Home Screen after Splash Screen*/
        Handler().postDelayed({
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        }, SPLASH_DELAY)
    }
}